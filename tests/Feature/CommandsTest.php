<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommandsTest extends TestCase
{

    public function commands_table_is_empty(): void
    {
        $response = $this->get('/admin');

        $response->assertStatus(200);

        $response->assertDontSee('Aucun résultat trouvé.');
    }
}
