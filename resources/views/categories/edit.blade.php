 @extends('layouts.admin')
 @section('title','Modifier une categorie')
 @section('content')
    <h1>Modifier la categorie num {{$cat->id}}</h1>
    <form action="{{route('categories.update',["category"=>$cat->id])}}" method="POST">
        @csrf
        @method('PUT')
            <div class="form-group">
              <label for="designation">Designation</label>
              <input type="text" class="form-control" id="designation" value="{{old('designation',$cat->designation)}}">
            </div>
            <div class="form-group">
              <label for="description">Description</label>
              <textarea type="text" class="form-control" id="description" cols="30" rows="10" value="{{old('designation',$cat->designation)}}">{{old('designation',$cat->description)}}</textarea>
            </div>
            <div>
                <input class="btn btn-primary mt-2" type="submit" value="Modifier">
            </div>
    </form>
    <div>
        @if($errors->any())
        <ul>
          @foreach($errors->all() as $er)
           <li>{{$er}}</li>

           @endforeach
        </ul>



        @endif
    </div>

@endsection
