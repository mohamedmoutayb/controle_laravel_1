 @extends('layouts.admin')
 @section('title','Gestion des categories')
 @section('content')

    <h1>Liste des categories</h1>
    <a class="btn btn-primary p-1" href="{{route('categories.create')}}">Ajouter une nouvelle categorie</a>
    <table class="table">
      <tr>
        <th>Id</th>
        <th>Designation</th>
        <th>Description</th>
        <th colspan="3">Actions</th>
      </tr>
      @foreach ($categories as $cat)
          <tr>
            <td>{{$cat->id}}</td>
            <td>{{$cat->designation}}</td>
            <td>{{$cat->description}}</td>
            <td><a class="btn btn-secondary p-1" href="{{route('categories.show',["category"=>$cat->id])}}">Details</a></td>
            <td><a class="btn btn-success p-1" href="{{route('categories.edit',["category"=>$cat->id])}}">Modifier</a></td>
            <td>
                <form action="{{route('categories.destroy',["category"=>$cat->id])}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <input class="btn btn-danger p-1" type="submit" value="Supprimer" onclick="return confirm('voulez-vous supprimer cette categorie?')">
                </form></td>
          </tr>
      @endforeach
    </table>
 @endsection
