@extends('layouts.admin')
@section('title','List des commmands')
@section('content')

<h1 class="my-2">Recherche des commandes</h1>
<form method="GET" action="{{ route('admin.index') }}">
    <div class="input-group m-3">
       <div class="input-group-prepend">
         <span class="input-group-text rounded-0 border border-secondary" id="basic-addon3">Nom</span>
       </div>
       <input class="col-xs-5 border border-secondary  div-inputs" type="text" class="form-control" name="nom" id="nom" value="{{ old('',$nom) }}" aria-describedby="basic-addon3">
       <div class="input-group-prepend ">
        <span class="input-group-text rounded-0 border border-secondary" id="basic-addon3">Entre la date: </span>
       </div>
       <input class="col-xs-5 border border-secondary " type="date" class="form-control" name="first_date" id="first_date" value="" aria-describedby="basic-addon3">
       <div class="input-group-prepend ">
          <span class="input-group-text rounded-0 border border-secondary" id="basic-addon3">et: </span>
        </div>
        <input class="col-xs-5 border border-secondary div-inputs" type="date" class="form-control" name="second_date" id="second_date" value="" aria-describedby="basic-addon3">
        <div class="input-group-prepend">
          <span class="input-group-text rounded-0 border border-secondary" id="basic-addon3">Etat</span>
         </div>
         <select class="col-xs-5 border border-secondary  div-inputs" class="form-control" aria-describedby="basic-addon3" name="etat" id="etat">
           <option value="" selected>Selectione...</option>
           <option value="En attente de confirmation" >En attente de confirmation</option>
           <option value="ِConfirmée" >ِConfirmée</option>
           <option value="Envoyé" >Envoyé</option>
           <option value="Payée" >Payée</option>
           <option value="Retournée" >Retournée</option>
         </select>
         <button class="btn btn-primary mx-3" type="submit">Rechercher</button>
    </div>
</form>


<h1>Liste des categories</h1>
<div class="button">
    <a class="btn btn-primary" href="{{ route('export') }}">Export</a>
</div>
<table class="table mt-3">
  <tr>
    <th>ID</th>
    <th>Nom</th>
    <th>Prenom</th>
    <th>Tele</th>
    <th>Adresse</th>
    <th>Ville</th>
    <th>Prix Totale</th>
    <th>Date du commande</th>
    <th>Etat</th>
    <th colspan="2">Actions</th>
  </tr>
  @forelse ($commandes as $commande)
      <tr>
        <td>{{$commande->id}}</td>
        <td>{{$commande->nom}}</td>
        <td>{{$commande->prenom}}</td>
        <td>{{$commande->tele}}</td>
        <td>{{$commande->adresse}}</td>
        <td>{{$commande->ville}}</td>
        <td>{{$commande->prix_total}}</td>
        <td>{{ \Carbon\Carbon::parse($commande->created_at)->format('m/d/Y') }}</td>
        <td>
            <span class ="@switch($commande->etat)
                @case('En attente de confirmation') confirmation @break
                @case('ِConfirmée') confirmed @break
                @case('Envoyé') sent @break
                @case('Payée') paid @break
                @case('Retournée') returned @break
                @default ''
                @endswitch">
                {{$commande->etat}}
            </span>
        </td>
        <td><a class="btn btn-secondary p-1" href="{{ route('admin.show',$commande->id) }}">Details</a></td>
        <td><a class="btn btn-success p-1" href="{{ route('admin.edit',$commande->id) }}">Modifier</a></td>
        {{-- <td>
            <form action="" method="POST">
                @method('DELETE')
                @csrf
                <input class="btn btn-danger p-1" type="submit" value="Supprimer" onclick="return confirm('voulez-vous supprimer cette categorie?')">
        </form></td> --}}
        {{-- <td>{{$commande->client->nom}}</td>
        <td>{{$commande->client->prenom}}</td>
        <td>{{$commande->client->tele}}</td>
        <td>{{$commande->client->adresse}}</td>
        <td>{{$commande->client->ville}}</td> --}}
            @empty
            <td class="text-center" colspan="12">Aucun résultat trouvé.</td>
      </tr>
  @endforelse
</table>

@endsection
