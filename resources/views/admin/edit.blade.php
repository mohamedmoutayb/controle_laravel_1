@extends('layouts.admin')
@section('title','Ajouter une categorie')
@section('content')

<form action="{{route('admin.update', $commande->id)}}" method="POST">
    @csrf
    @method('PUT')
    <h3>Modification du commande N°{{ $commande->id }}</h3>
    <div class="form-group">
        <label class="mt-2" for="etat">Etat</label>
        <select   name="etat" class="form-control" id="etat">
          <option >{{ old('',$commande->etat)}}</option>
          <option >En attente de confirmation</option>
          <option >ِConfirmée</option>
          <option >Envoyé</option>
          <option >Payée</option>
          <option >Retournée</option>
        </select>
        <div class="form-group">
          <label for="prenom">Nom Complet</label>
          <input disabled type="text"  class="form-control" id="prenom" value="{{ old('',$commande->nom. " " . $commande->prenom) }}">
        </div>
        <div class="form-group">
          <label for="tele">Téléphone</label>
          <input disabled type="text" class="form-control" id="tele" value="{{ old('',$commande->tele) }}">
        </div>
        <div class="form-group">
          <label for="adresse">Adresse</label>
          <input disabled type="text" class="form-control" id="adresse" value="{{ old('',$commande->adresse) }}">
        </div>
        <div class="form-group">
          <label for="ville">Ville</label>
          <input disabled type="text" class="form-control" id="ville" value="{{ old('',$commande->ville) }}">
        </div>
        <div class="form-group">
            <label for="prix_total">Prix totale</label>
            <input disabled type="text" class="form-control mb-2" id="prix_total" value="{{ old('',$commande->prix_total) }}">
        </div>
    </div>
    <div>
        <input type="submit" class="btn btn-primary" value="Modifier">
    </div>
</form>


@endsection
{{-- <td>{{$commande->id}}</td>
<td>{{$commande->etat}}</td>
<td>{{$commande->prix_total}}</td>
<td>{{$commande->nom}}</td>
<td>{{$commande->prenom}}</td>
<td>{{$commande->tele}}</td>
<td>{{$commande->adresse}}</td>
<td>{{$commande->ville}}</td> --}}
