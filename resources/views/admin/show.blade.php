@extends('layouts.admin')
@section('title','List des commmands')
@section('content')

<a class="btn btn-primary p-1 mb-3" href="{{route('admin.index')}}">Retourner vers la liste des commandes</a>
<h1>Detail du commande Num {{$commande->id}}</h1>
<div>
    <p><strong>Nom Complet:</strong> {{$commande->nom ." ". $commande->prenom}}</p>
    <p><strong>Téléphone:</strong> {{$commande->tele}}</p>
    <p><strong>Adresse:</strong> {{$commande->adresse}}</p>
    <p><strong>Ville:</strong> {{$commande->ville}}</p>
    <p><strong>Prix totale:</strong> {{$commande->prix_total}}</p>
    <p><strong>Date du commande:</strong> {{ \Carbon\Carbon::parse($commande->created_at)->format('m/d/Y') }}</p>
    <p><strong>Description:</strong></p>
     <ul>
        <li style="list-style-type: none">
            {!! $commande->description  !!}
        </li>
    </ul>
</div>

@endsection
