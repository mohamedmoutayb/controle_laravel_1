@extends('layouts.admin')
@section('title', 'Gestion des categories')
@section('content')

<style>
    .catalogue {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
    }

    .item {
        width: 300px;
        border: 1px solid #ddd;
        border-radius: 8px;
        overflow: hidden;
        margin: 10px;
        transition: transform 0.3s ease-in-out;
        text-align: center;
        background-color: #fff;
    }

    .item:hover {
        transform: scale(1.05);
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    }

    .item img {
        width: 100%;
        height: 200px;
        object-fit: cover;
        border-top-left-radius: 8px;
        border-top-right-radius: 8px;
    }

    .item-content {
        padding: 15px;
    }

    .item p {
        margin: 5px 0;
        font-size: 14px;
        color: #555;
    }

    .item-title {
        font-size: 18px;
        font-weight: bold;
        margin-bottom: 10px;
    }

    .btn-primary {
        background-color: #1e00ff;
        border-color: #1e00ff;
        color: #fff;
        padding: 8px 16px;
        border-radius: 4px;
        cursor: pointer;
        transition: background-color 0.3s;
    }

    .btn-primary:hover {
        background-color: #1e00ff;
        border-color: #1e00ff;
    }
</style>

<div class="container my-4">
    <h1 class="mb-4">Liste des categories</h1>

    <div class="catalogue">
        @foreach ($produits as $item)
        <div class="item">
            <!-- Display product image if available -->
            @if (isset($item->photo))
            <img src="{{ asset('storage/' . $item->photo) }}" alt="{{ $item->designation }}">
            @endif
            <div class="item-content">
                <p class="item-title">{{ $item->designation }}</p>
                <p>Prix : {{ $item->prix_u }} MAD</p>

                @if ($item->quantite_stock == 0)
                <p class="text-danger">En rupture de stock</p>
                @else
                <p>En stock : {{ $item->quantite_stock }}</p>
                <form action="{{ route('home.add', ['id' => $item->id]) }}" method="POST">
                    @csrf
                    <label for="qte">Quantite</label>
                    <input type="number" name="qte" id="qte" min="1" max="{{ $item->quantite_stock }}">
                    <input type="submit" class="btn btn-primary" value="Acheter">
                </form>
                @endif
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection
