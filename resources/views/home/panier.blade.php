@extends('layouts.admin')
@section('title', 'Gestion des categories')
@section('content')

<style>
    h1 {
        text-align: center;
        margin-bottom: 20px;
    }

    .main {
        max-width: 800px;
        margin: 20px auto;
        padding: 20px;
        background-color: #ffffff; /* White background color for the main content */
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }

    table {
        width: 100%;
        border-collapse: collapse;
        margin-top: 20px;
    }

    th, td {
        border: 1px solid #dee2e6;
        padding: 15px;
        text-align: center;
    }

    th {
        background-color: #f8f9fa;
    }

    .total-row {
        font-weight: bold;
    }

    .actions {
        display: flex;
        justify-content: space-between;
        margin-top: 20px;
    }

    .btn {
        padding: 12px 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        border-radius: 6px;
        transition-duration: 0.4s;
        cursor: pointer;
    }

    .btn-commander {
        background-color: #28a745;
        color: white;
        border: 2px solid #28a745;
    }

    .btn-commander:hover {
        background-color: #218838;
        color: white;
    }

    .btn-vider {
        background-color: #dc3545;
        color: white;
        border: 2px solid #dc3545;
    }

    .btn-vider:hover {
        background-color: #c82333;
        color: white;
    }

    .btn-supprimer {
        background-color: #dc3545;
        color: white;
        border: 2px solid #dc3545;
    }

    .btn-supprimer:hover {
        background-color: #c82333;
        color: white;
    }
</style>

@if (empty($panier))
    <p>Votre panier est vide</p>
@else
    <h1>Mon panier</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Designation</th>
                <th>Prix unitaire</th>
                <th>Quantite</th>
                <th>Total ligne</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($panier as $id => $item)
            <tr>
                <td>{{ $id }}</td>
                <td>{{ $item['produit']->designation }}</td>
                <td>{{ $item['produit']->prix_u }} MAD</td>
                <td>{{ $item['qte'] }}</td>
                <td>{{ $item['qte'] * $item['produit']->prix_u }} MAD</td>
                <td>
                    <form action="{{ route('home.delete', ['id' => $id]) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-supprimer"
                            onclick="return confirm('Voulez-vous supprimer cette ligne?')">Supprimer</button>
                    </form>
                </td>
            </tr>
            @endforeach
            <tr class="total-row">
                <th colspan="4">Total</th>
                <td colspan="2">{{ $tot }} MAD</td>
            </tr>
        </tbody>
    </table>

    <div class="actions">
        <a href="{{ route('home.commander') }}" class="btn btn-commander">Commander</a>
        <a href="{{ route('home.clear') }}" class="btn btn-vider">Vider le panier</a>
    </div>
@endif

@endsection
