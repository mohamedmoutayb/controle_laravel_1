@extends('layouts.admin')
@section('title', 'Gestion des categories')
@section('content')

<style>
    h1 {
        text-align: center;
        margin-bottom: 30px;
    }

    form {
        max-width: 600px;
        margin: 0 auto;
        background-color: #f8f9fa;
        padding: 20px;
        border-radius: 10px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }

    label {
        display: block;
        margin-bottom: 8px;
    }

    input,
    textarea {
        width: 100%;
        padding: 10px;
        margin-bottom: 16px;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
    }

    button {
        background-color: #007bff;
        color: #fff;
        padding: 12px 20px;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        font-size: 16px;
        transition: background-color 0.3s;
    }

    button:hover {
        background-color: #0056b3;
    }

    /* .item-label {
        font-size: 20px;
        margin-top: 20px;
        font-weight: bold;
    } */
</style>

<h1>Validation de la Commande</h1>

<form method="post" action="{{ route('home.storeClientCommande') }}">
    @csrf

    <!-- Client information fields -->
    <label for="nom">Nom:</label>
    <input type="text" name="nom" required>

    <label for="prenom">Prenom:</label>
    <input type="text" name="prenom" required>

    <label for="tele">Telephone:</label>
    <input type="text" name="tele" required>

    <label for="ville">Ville:</label>
    <input type="text" name="ville" required>

    <label for="adresse">Adresse:</label>
    <textarea name="adresse" rows="4" required></textarea> <br>

    @foreach($commandes as $index => $commande)
    <input type="hidden" name="commandes[{{ $index }}][designation]" value="{{ $commande['designation'] }}">
    <input type="hidden" name="commandes[{{ $index }}][totalItem]" value="{{ $commande['totalItem'] }}">
    <input type="hidden" name="commandes[{{ $index }}][qte]" value="{{ $commande['qte'] }}">
   @endforeach
    <label for="prix_total">Prix total:</label>
    <input type="text" name="prix_total" value="{{ $tot }}" readonly>

    <!-- Submit button -->
    <button type="submit">Commander</button>
</form>

@endsection
