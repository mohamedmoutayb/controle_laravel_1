<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategorieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[HomeController::class,"index"])->name('home.index');
Route::match(['get', 'post'], '/panier/commander/save', [HomeController::class, "storeClientCommande"])->name('home.storeClientCommande');
Route::get('/panier',[HomeController::class,"show_panier"])->name('home.panier');
Route::get('/panier/commander',[HomeController::class,"commander"])->name('home.commander');
Route::get('/panier/clear',[HomeController::class,"clear"])->name('home.clear');
Route::post('panier/add/{id}',[HomeController::class,"add"])->name('home.add');
Route::delete('panier/delete/{id}',[HomeController::class,"delete"])->name('home.delete');
Route::resource("categories",CategorieController::class);
Route::resource("admin",AdminController::class);
Route::get('/admin/search', [AdminController::class,'search'])->name('admin.search');
Route::get('/export-excel', [AdminController::class,'export'])->name('export');

