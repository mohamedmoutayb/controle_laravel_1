<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Produit;
use App\Models\Commande;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){

        $produits=Produit::all([ 'id','designation','prix_u','quantite_stock','photo']);
        return view('home.index',compact('produits'));
    }
    public function add(Request $request,$id){
        $qte=$request->input('qte',1);
        $produit=Produit::find($id);
        $panier=$request->session()->get('panier',[]);
        if(isset($panier[$id])){
            $panier[$id]['qte']=$qte;
        }else{
        $panier[$id]=[
            'qte'=>$qte,
             'produit'=>$produit
        ];
    }

    $request->session()->put('panier',$panier);
    return redirect()->back();

    }



    public function show_panier(Request $request){
        $panier=$request->session()->get('panier',[]);
        $tot=0;
        foreach($panier as $id=>$item){
            $tot+=$item['qte']*$item['produit']->prix_u;
        }
        return view('home.panier',compact('panier','tot'));
    }



    public function delete(Request $request,$id){
        $panier=$request->session()->get('panier',[]);
        unset($panier[$id]);
        $request->session()->put('panier',$panier);
        return redirect()->back();
    }



    public function clear(Request $request ){

        $request->session()->forget('panier');
        // return redirect()->back();
        return redirect()->route('home.index');

    }


    public function commander(Request $request){
        $panier = $request->session()->get('panier', []);
        $commandes = [];
        $tot = 0;

        foreach ($panier as $id => $item) {
            $designation = $item['produit']->designation;
            $qte = $item['qte'];
            $totalItem = $qte * $item['produit']->prix_u;
            $tot += $totalItem;

            // Concatenate information for each item and store in an array
            $commandes[] = [
                'designation' => $designation,
                'qte' => $qte,
                'totalItem' => $totalItem,
            ];
        }

        return view('home.commander', compact('tot', 'commandes'));
    }


    public function storeClientCommande(Request $request) {
        $clientData = $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'tele' => 'required',
            'ville' => 'required',
            'adresse' => 'required',
        ]);

        $commandData = $request->validate([
            'prix_total' => 'required',
        ]);

        // try {
            // DB::transaction(function () use ($clientData, $commandData) {
                $client = Client::create($clientData);

                $clientId = $client->id;
                $clientName = $client->nom . ' ' . $client->prenom;
                $dateTime = now();

                $commandData['client_id'] = $clientId;
                $commandData['date_time'] = $dateTime;
                $commandData['etat'] = 'En attente de confirmation';
                 $commandData['description'] = $this->JoinItems($request->input('commandes'));

                 Commande::create($commandData);
            // });

               return view('home.mycommandes');
        // } catch (\Exception $ex) {
        //     return response()->json(['error' . $ex->getMessage()], 500);
        // }
    }

    /**
     * Serialize items into a custom string format
     */
    private function JoinItems($items)
    {
        $joinededItems = [];

        foreach ($items as $item) {
            $designation = $item['designation'];
            $prix = $item['totalItem'];
            $qte = $item['qte'];

            $joinededItems[] = "<strong>Produit:</strong> $designation, <strong>Prix:</strong> $prix, <strong>Qte:</strong> $qte";
        }

        return implode(" <br> ", $joinededItems);
    }
}
