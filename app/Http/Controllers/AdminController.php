<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Client;
use App\Models\Commande;
use Illuminate\Http\Request;
use App\Exports\CommandesExport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller

{
    protected $query;

    public function __construct()
    {
        $this->query = DB::table('commandes')
            ->join('clients', 'commandes.client_id', '=', 'clients.id')
            ->select('commandes.*', 'clients.*');
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $nom = $request->input('nom');
        $firstDate = $request->input('first_date');
        $secondDate = $request->input('second_date');
        $etat = $request->input('etat');

        // $commandes = Commande::with('client')->get();
        if ($nom) {
            $this->query->where('nom', 'like', '%' . $nom . '%');
        }

        if ($firstDate && $secondDate) {
            $this->query->whereBetween('commandes.created_at', [$firstDate,$secondDate]);
        }

        if ($etat) {
            $this->query->where('etat', $etat);
        }

        $commandes = $this->query->get();

        return view("admin.index", compact('commandes', 'nom'));

    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {



    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $commande = $this->query->where('commandes.client_id' , '=' , $id)->first();

        return view('admin.show', compact('commande'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $commande = $this->query->where('commandes.client_id' , '=' , $id)->first();

        return view('admin.edit', compact('commande'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $etat = $request->input('etat');

        DB::table('commandes')->where('client_id', $id)->update(['etat' => $etat]);

        return redirect()->route('admin.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
    public function export() {
        return Excel::download(new CommandesExport(), 'export1.xlsx');
    }
}












// public function index()
// {
//     // $commandes = Commande::with('client')->get();
//     $commandes = DB::table('commandes')
//     ->join('clients', 'commandes.client_id', '=', 'clients.id')->select('commandes.*', 'clients.*')
//     ->get();
//     return view("admin.index", compact('commandes'));

// }


// public function search(Request $request)
// {
//     $nom = $request->input('nom');
//     // $firstDate = $request->input('first_date');
//     // $secondDate = $request->input('second_date');
//     // $etat = $request->input('etat');

//     $query = Command::query();

//     if ($nom) {
//         $query->where('nom', 'like', '%' . $nom . '%');
//     }

//     // if ($firstDate && $secondDate) {
//     //     $query->whereBetween('created_at', [$firstDate,$secondDate]);
//     // }

//     // if ($etat) {
//     //     $query->where('etat', $etat);
//     // }

//     $commandes = $query->get();
//     //dd($commandes);

//      return view('admin.index', compact('commandes', 'nom'));
// }
