<?php

namespace App\Models;
use App\Models\Client;
//use App\Models\Produit;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    use HasFactory;
    protected $tableName = "commandes";
    protected $fillable= [
        'client_id',
        'date_time',
        'description',
        'prix_total'
    ];
    protected $attributes = [
        'etat' => 'En attente de confirmation',
    ];

    public function client()
    {
    return $this->belongsTo(Client::class);
     }
}
