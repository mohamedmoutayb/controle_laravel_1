<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Commande;

class CommandesExport implements FromCollection, WithHeadings
{
    public function collection()
    {

        $commandes = Commande::with('client')->get()->map(function ($commande) {
            return [
                'id' => $commande->id,
                'etat' => $commande->etat,
                'client_nom' => $commande->client->nom,
                'client_prenom' => $commande->client->prenom,
                'client_tele' => $commande->client->tele,
                'client_ville' => $commande->client->ville,
                'client_adresse' => $commande->client->adresse,
            ];
        });

        return $commandes;
    }

    public function headings(): array
    {

        return [
            'id',
            'etat',
            'client_nom',
            'client_prenom',
            'client_tele',
            'client_ville',
            'client_adresse',
        ];
    }
}
